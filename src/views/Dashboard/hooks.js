import server from '../../feathers-client';
import _ from 'underscore';

const useHooks = () => {
    return {
        fetchUsers: async () => {
            try {
                const userService = server.service('users');
                const userList = await userService.find({query: {isDeleted: false, $limit: 20, $sort: {
                    createdAt: -1,
                }}});

                return userList.data;
            } catch (error) {
                throw new Error(error);
            }
        },
        deleteUser: async (id) => {
            try {
                const userService = server.service('users');
                const onDelete = await userService.patch(id, {isDeleted: true});

                return onDelete;
            } catch (error) {
                throw new Error(error);
            }
        }
    }
}

export default useHooks;