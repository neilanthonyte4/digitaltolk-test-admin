import server from '../feathers-client';

const useAuthHook = () => {
    return {
        reAuth: async () => {
            try {
                const userService = server.service('users');
                const reAuth = await server.authenticate();
                const loginUserInfo = await userService.get(reAuth.user._id);

                return loginUserInfo;
            } catch (error) {
                throw new Error(error);
            }
        }
    }
}

export default useAuthHook;